# docker-serviio

[![](http://serviio.org/images/serviio.png)](http://serviio.org/) 

This docker-serviio works on a Raspberry Pi.

### Build Args

 - **VERSION** = 2.0 - serviio version

### Exposed Ports

 - **1901:1901/udp**
 - **1900:1900/udp**
 - **8895:8895/tcp**
 - **23423:23423/tcp** - HTTP/1.1 /console /rest
 - **23523:23523/tcp** - HTTPS/1.1 /console /rest
 - **23424:23424/tcp** - HTTP/1.1 /cds /mediabrowser
 - **23524:23524/tcp** - HTTPS/1.1 /cds /mediabrowser

### Volumes
 - **/opt/serviio/library**
 - **/opt/serviio/plugins**
 - **/opt/serviio/log**
 - **/media** - put media content here and add in serviio gui this path


### Container Changelog (dd.mm.yy)
 - **05.01.2020** - Update Serviio version to 2.0 and debian to latest. Custom FFMPEG removed, YASM removed, NASM removed, default x264 library, openjdk-8-jre instead oracle-java used.

### Starting

Project forked from https://github.com/riftbit/docker-serviio

VERSION               0.3

```
docker create \
        --name=serviio \
        -v /media/big/pelis:/media \
        -v /opt/serviio-2.0/library:/opt/serviio/library \
        -v /opt/serviio-2.0/plugins:/opt/serviio/plugins \
        -v /opt/serviio-2.0/log:/opt/serviio/log \
        -p 23423:23423/tcp \
        -p 23424:23424/tcp \
        -p 8895:8895/tcp \
        -p 1900:1900/udp \
        -p 1901:1901/udp \
        -p 1901:1901/tcp \
        -p 5000:5000/tcp \
        -p 5000:5000/udp \
        --net=host \
        --restart unless-stopped \
        jfrs/debian-serviio
```
