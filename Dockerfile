# Serviio docker
#
# Project forked from https://github.com/riftbit/docker-serviio
#
# VERSION               0.3
#
# Create image with:
#docker build -t jfrs/docker-serviio:v2 .
#
# Run with: 
#docker create \
#        --name=serviio \
#        -v /media/max/serviio:/mediamax \
#        -v /media/big/pelis:/media \
#        -v /media/pi/GB32:/mediapi \
#        -v /opt/serviio-2.0/library:/opt/serviio/library \
#        -v /opt/serviio-2.0/plugins:/opt/serviio/plugins \
#        -v /opt/serviio-2.0/log:/opt/serviio/log \
#        -p 23423:23423/tcp \
#        -p 23424:23424/tcp \
#        -p 8895:8895/tcp \
#        -p 1900:1900/udp \
#        -p 1901:1901/udp \
#        -p 1901:1901/tcp \
#        -p 5000:5000/tcp \
#        -p 5000:5000/udp \
#        --net=host \
#        --restart unless-stopped \
#        jfrs/debian-serviio:v2

FROM debian:buster-slim

# Install Dependencies
RUN apt-get update && apt-get upgrade -y 

RUN mkdir -p /usr/share/man/man1/
RUN apt-get install -y openjdk-11-jre x264 ffmpeg curl tar

ARG SERVIIO_VERSION=2.0

# Install Serviio
CMD cd $HOME/src 
RUN curl -s http://download.serviio.org/releases/serviio-${SERVIIO_VERSION}-linux.tar.gz | tar zxf - -C . 
RUN mkdir -p /opt/serviio && \
  mv ./serviio-${SERVIIO_VERSION}/* /opt/serviio && \
  chmod +x /opt/serviio/bin/serviio.sh && \
  rm -rf $HOME/src

VOLUME ["/opt/serviio/library", "/opt/serviio/plugins", "/opt/serviio/log", "/media", "/mediapi"]

EXPOSE 23423:23423/tcp
EXPOSE 23424:23424/tcp
EXPOSE 23425:23425/tcp
EXPOSE 8895:8895/tcp
EXPOSE 1900:1900/udp
EXPOSE 1901:1901/udp
EXPOSE 5000:5000/udp
EXPOSE 5000:5000/tcp

CMD /opt/serviio/bin/serviio.sh

